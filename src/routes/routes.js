const { Router } = require("express");
const router = Router();
const ProductController = require("../controllers/ProductController");
const UserController = require("../controllers/UserController");
const CartController = require('../controllers/CartController')

router.post("/User", UserController.create);
router.get("/User /:id", UserController.show); 
router.get("/User", UserController.index); 
router.put("/User/:id", UserController.update);
router.delete("/User/:id", UserController.destroy);

router.post("/Product", ProductController.create);
router.get("/Product/:id", ProductController.show); 
router.get("/Product", ProductController.index); 
router.put("/Product/:id", ProductController.update);
router.delete("/Product/:id", ProductController.destroy);

router.post('/Cart', CartController.create);
router.get('/Cart/:id', CartController.show)
router.get('/Cart', CartController.index)
router.put('/Cart/:id', CartController.update)
router.delete('/Cart/:id', CartController.destroy)


module.exports = router;
