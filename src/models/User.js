const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    birthday: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cpf: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cep: {
        type:DataTypes.STRING,
        allowNull: false
    },
    hash: {
        type: DataTypes.STRING,
    },
     salt: {
         type: DataTypes.STRING,
    }
}
    , {
    timestamps: false
});

User.associate = function(models) {
    User.hasOne(models.Cart);
};

module.exports = User;