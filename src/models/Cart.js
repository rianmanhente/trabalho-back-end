const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Cart = sequelize.define('Cart', {
    the_amount: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
   
}, {
    timestamps: false
});

Cart.associate = function (models) {
    Cart.belongsTo(models.User);
    Cart.hasOne(models.Product)
    
};


module.exports = Cart;