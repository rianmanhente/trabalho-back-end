const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    the_amount: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    evaluation: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    payment_method: {
        type: DataTypes.STRING,
        allowNull: false
    },
    photograph: {
        type: DataTypes.TEXT,
        allowNull: false
    }
}, {
    timestamps: false
});

Product.associate = function (models) {
   // Product.belongsToMany(models.Cart , through: 'Order', as: 'productld');
    Product.belongsToMany(models.Cart,{through: 'Order', as: 'productId'})
};


module.exports = Product;