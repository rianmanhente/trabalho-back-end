const User = require('../models/User')



 const create = async(req,res) => {
    try {
        const user = await User.create(req.body)
        return res.status(201).json({ message: "Cliente cadastrado com sucesso!", user: User});
    }
    catch(err){
        return
        res.status(500).json({error: err}); 

    }
};

const index = async(req, res) =>{
    try{
        const user = await User.findAll();
        return res.status(200).json({user});
    } catch(err){
        return res.status(500).json({err});
    }
};


// async function read(req,res) {
//     try {
//         const User
//         const 
//     }
// }


const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({error});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await User.update(req.body, {where: {id: id}});
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Usuário não foi encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(error){
        return res.status(500).json("Usuário não foi encontrado.");
    }
};

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
}
